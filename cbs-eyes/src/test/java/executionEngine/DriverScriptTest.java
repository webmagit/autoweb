package executionEngine;

import org.junit.Before;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import parent.Base;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;
import tests.groups.Driver;
import utilities.Config;
import utilities.GlobalLog;
import utilities.Log;
import utilities.SheetsUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by kanak.kalburgi on 7/04/2016.
 */
@RunWith(Parameterized.class)
@Category(Driver.class)
public class DriverScriptTest extends Base {

    private int noOfTestCases;
    private int noOfTestSteps;
    public static boolean testResult = true;
    public static boolean stepExecutionEnabled = true;
    public static String component;
    public static String actionKeyword;
    public static String testStepDescription;
    public static int testStepStart;
    public static String lastStepAction = "";

    @Parameterized.Parameters(name="{0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { Config.runTest }
        });
    }

    private String scenario;
    public DriverScriptTest(String scenario) {
        this.scenario = scenario;
    }

    @Before
    public void testSetup() {

    }

    @Features("Data Engine")
    @Stories({"Scenario Execution", "Sheets Interaction"})
//    @Title("Execute Scenario")
    @Description("Each Scenario has one or more Steps")
    @Severity(SeverityLevel.NORMAL)
    @Test
    public void execute() throws Exception {

        String stepCondition;
        String ageFormula;
        int testCaseID;
        String testCase = Config.runTest;
        boolean testCaseFound = false;

        final EnvironmentVariables environmentVariables = new EnvironmentVariables();

        SheetsUtils.setSheet();

        testStepStart = SheetsUtils.getRowContainingValue(Config.TestStepsWorkSheet, Config.TestCaseIDColumn, testCase);

        String whatReallyHappened;

        String expectedToHappen = "Test Case " + testCase + " Found!";

        if (testStepStart == -1) {

            GlobalLog.error("Test Case not found: " + testCase);
            environmentVariables.set("testCaseFound", "NONE");
            whatReallyHappened = "Test Case " + testCase + " Not Found!";

        } else {

            environmentVariables.set("testCaseFound", "FOUND");
            whatReallyHappened = expectedToHappen;

        }

        assertThat(whatReallyHappened, is(expectedToHappen));

        GlobalLog.info("Found Test Case " + testCase + " at ROW # " + testStepStart);
        int lastTestCaseRow = SheetsUtils.getTestStepEndRow(Config.TestStepsWorkSheet, testCase, testStepStart);
        GlobalLog.info("Testcase " + testCase + " ROWS " + testStepStart + "-" + lastTestCaseRow);

        environmentVariables.set("lastTestCaseRow", Integer.toString(lastTestCaseRow));

        for (;testStepStart<=lastTestCaseRow;testStepStart++) {
            String stepName;

            stepCondition = SheetsUtils.getCellValue(Config.TestStepsWorkSheet, testStepStart, Config.TestStepConditionColumn);

            ageFormula = SheetsUtils.getAgeFormula("", false);
            stepName = SheetsUtils.getCellValue(Config.TestStepsWorkSheet, testStepStart, Config.TestStepsColumn);

            if ( stepCondition.equalsIgnoreCase(Config.TestStep_Keyword_SKIP)) {

                GlobalLog.info("# SKIPPING " + testStepStart + ":" + stepName + " #");
                SheetsUtils.setCellValue(Config.TestStepsWorkSheet, testStepStart, Config.getTestStepResultsColumn(), Config.TestStep_Keyword_SKIP);
                SheetsUtils.setCellValue(Config.TestStepsWorkSheet, testStepStart, Config.getStepAgeColumn(), ageFormula);

                continue;
            }

            stepName = SheetsUtils.getCellValue(Config.TestStepsWorkSheet, testStepStart, Config.TestStepsColumn);
            component = SheetsUtils.getCellValue(Config.TestStepsWorkSheet, testStepStart, Config.TestStepPageObjectColumn);
            actionKeyword = SheetsUtils.getCellValue(Config.TestStepsWorkSheet, testStepStart, Config.TestStepActionKeywordColumn);
            testStepDescription = SheetsUtils.getCellValue(Config.TestStepsWorkSheet, testStepStart, Config.TestStepDescription);

            GlobalLog.info("* SELECTED " + testStepStart + ":" + stepName + " *"
                    + " | " + "Description: "
                            + testStepDescription
                    + " | "
                    );

            executeActions();

            if (testResult == false) {
                //TODO: for mandatory steps, break here and mark rest of the steps as "skipped"
                if (stepCondition.equalsIgnoreCase(Config.TestStep_Keyword_OPTIONAL)) {
                    continue;
                } else { // default=MANDATORY
                    // do not execute rest of the steps
                    if (lastStepAction == "") {
                        lastStepAction = stepName;
                        Log.printConsole("Setting lastStepAction to " + lastStepAction, true);
                    }

                    stepExecutionEnabled = false;
                }

            }

        } // executed all the test steps of the test case

    }

    private void executeActions() throws Exception {

        int testStepResultsColumn = Config.getTestStepResultsColumn();

        if (stepExecutionEnabled == true) {

            String requiredClass = Config.PageObjectsPackage + "." + component;

            Class<?> selectedClass = Class.forName(requiredClass);
            Constructor<?> constructor = selectedClass.getConstructor(WebDriver.class);
            Object instance = constructor.newInstance(driver);
            Method[] dynamicMethods = selectedClass.getDeclaredMethods();

            for (Method method : dynamicMethods) {

                String theMethod = method.getName();
                Log.printConsole("Discovered method = " + theMethod, true);

                if (theMethod.equals(actionKeyword)) {
                    SheetsUtils.setCellValue(Config.TestStepsWorkSheet, testStepStart, testStepResultsColumn, Config.TestStep_Keyword_IN_PROGRESS);
                    SheetsUtils.setCellValue(Config.TestStepsWorkSheet, testStepStart, Config.getStepAgeColumn(), SheetsUtils.getAgeFormula(Config.TestExecutionInProgress, false));

                    testResult = true; // reset before execution

                    GlobalLog.info(component + " -> " + theMethod + " # STARTING #");
                    method.invoke(instance, component, driver, eyes);
                    GlobalLog.info(component + " -> " + theMethod + " # FINISHED #");

                    String ageFormula = SheetsUtils.getAgeFormula("", false); // for the hyperlink without the '='

                    String displayText = "";

                    if (testResult == true) {

                        displayText = Config.TestStep_Keyword_PASS;


                    } else if (testResult == false) {

                        displayText = Config.TestStep_Keyword_FAIL;


                    }

                    SheetsUtils.setCellValue(Config.TestStepsWorkSheet, testStepStart, testStepResultsColumn, displayText);
                    SheetsUtils.setCellValue(Config.TestStepsWorkSheet, testStepStart, Config.getStepAgeColumn(), ageFormula);
                }
            }       //TODO: if no methods match, then flag as "not implemented"
        } else {
            SheetsUtils.setCellValue(Config.TestStepsWorkSheet, testStepStart, testStepResultsColumn, Config.TestStep_Keyword_WONT_EXECUTE);
            SheetsUtils.setCellValue(Config.TestStepsWorkSheet, testStepStart, Config.getStepAgeColumn(), lastStepAction + " FAILED!");
        }
    }
}
