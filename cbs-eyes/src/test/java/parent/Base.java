package parent;

import com.applitools.eyes.*;
import com.saucelabs.saucerest.SauceREST;
import org.junit.*;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.rules.ExternalResource;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import ru.yandex.qatools.allure.annotations.Attachment;
import utilities.*;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static utilities.Config.*;


/**
 * Created by Kanak on 13/02/2016.
 */
public class Base {

    protected WebDriver driver;
    protected String testName;
    protected String sessionID;
    private SauceREST sauceClient;
    protected Eyes eyes;

    protected String JUnitSessionUniqueID = "";

    String browser = "";
    String baseURL = "";
    String host = "";
    String browserVersion = "";
    String device = "";
    String deviceOrientation = "";
    String platform = "";
    String activeProfile = "";
    String screenResolution = "";
    String environment = "";

    @BeforeClass
    public static void setSomeStuff() throws IOException {

        Config.setPlatformConfiguration();
    }


    @Rule
    public ExternalResource resource = new ExternalResource() {
        @Override
        protected void before() throws Throwable {

            browser = System.getenv("browser");
            baseURL = System.getenv("baseURL");
            host = System.getenv("host");
            browserVersion = System.getenv("browserVersion");
            device = System.getenv("device");
            deviceOrientation = System.getenv("deviceOrientation");
            platform = System.getenv("platform");
            screenResolution = System.getenv("screenResolution");

            activeProfile = System.getenv("activeProfile");

            if (host.equals("saucelabs")) {

                DesiredCapabilities capabilities = new DesiredCapabilities();

                LoggingPreferences loggingprefs = new LoggingPreferences();
                loggingprefs.enable(LogType.BROWSER, Level.ALL);
                capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);

                String whatReallyHappened;
                String expectedToHappen = "Maven Profile Found";

                if ( activeProfile.equals("profile-1")) {

                    capabilities.setCapability("browserName", browser);
                    capabilities.setCapability("version", browserVersion);
                    capabilities.setCapability("platform", platform);
                    capabilities.setCapability("screenResolution", screenResolution);

                    whatReallyHappened = expectedToHappen;

                } else if ( activeProfile.equals("profile-9")) {

                    capabilities = DesiredCapabilities.iphone();
                    capabilities.setCapability("platform", platform);
                    capabilities.setCapability("version", "9.2");
                    capabilities.setCapability("deviceName", device);
                    capabilities.setCapability("deviceOrientation", deviceOrientation);
                    capabilities.setCapability("screenResolution", screenResolution);

                    whatReallyHappened = expectedToHappen;

                } else {

                    whatReallyHappened = "No maven profiles found (or activated)";

                }

                assertThat(whatReallyHappened, is(expectedToHappen));

                capabilities.setCapability("name", testName);

                String sauceUrl = String.format(
                        "http://%s:%s@ondemand.saucelabs.com:80/wd/hub",
                        sauceUser, sauceKey);


                WebDriver sauceBrowser = new RemoteWebDriver(new URL(sauceUrl), capabilities);
                sessionID = ((RemoteWebDriver) sauceBrowser).getSessionId().toString();
                sauceClient = new SauceREST(sauceUser, sauceKey);

                eyes = new Eyes();
                eyes.setApiKey(applitoolsKey);
                eyes.setBaselineName(testName);
                eyes.setMatchLevel(MatchLevel.LAYOUT2);
                eyes.setForceFullPageScreenshot(true);
                eyes.setStitchMode(StitchMode.CSS);
                eyes.setHideScrollbars(true);

                driver = eyes.open(sauceBrowser, "CBS Fantasy", testName);

            } else if (host.equals("localhost")) {

                if (browser.equals("firefox")) {
                    driver = new FirefoxDriver();
                    Log.printConsole("Opened Firefox", true);
                } else if (browser.equals("chrome")) {
                    System.setProperty("webdriver.chrome.driver",
                            System.getProperty("user.dir") + "/vendor/chromedriver.exe");
                    driver = new ChromeDriver();
                    Log.printConsole("Opened Chrome", true);
                }

                eyes = new Eyes();
                eyes.setApiKey(applitoolsKey);
                eyes.setBaselineName(testName);
                eyes.setMatchLevel(MatchLevel.LAYOUT2);
                eyes.setForceFullPageScreenshot(true);
                eyes.setStitchMode(StitchMode.CSS);

                driver = eyes.open(driver, "CBS SPORTS", testName, new RectangleSize(pageWidth, pageHeight));

            }
        }

        @Override
        protected void after() {

            try {
                eyes.close();
            } catch (Exception e) {
//                Log.info("EYES instance may already have been closed");
            }

            eyes.abortIfNotClosed();
            driver.quit();

        }
    };

    @Rule
    public TestRule watcher = new TestWatcher() {
        @Override
        protected void starting(Description description) {
            testName = description.getDisplayName();
//            description.getMethodName();
            JUnitSessionUniqueID = Config.randomID;

//            Log.info("Base generated -- " + JUnitSessionUniqueID + " for TEST ==>" + testName);

        }

        @Override
        protected void succeeded(Description description) {
            if (host.equals("saucelabs")) {
                sauceClient.jobPassed(sessionID);
            }
        }

        @Override
        protected void failed(Throwable throwable, Description description) {
            if (host.equals("saucelabs")) {
                sauceClient.jobFailed(sessionID);
                String sauceSessionURL = sauceSessionHost + sessionID;
                System.out.println(sauceSessionURL);
            }
        }
    };


    @Rule
    public final EnvironmentVariables environmentVariables
            = new EnvironmentVariables();

    @Before
    public void setEnvironmentVariable() throws IOException {

        environmentVariables.set("TestName", testName);
        environmentVariables.set("TestSessionUniqueID", JUnitSessionUniqueID);
        environmentVariables.set("testCaseFound", "NOT YET");
        environmentVariables.set("lastTestCaseRow", "-1");

        String logFileName = System.getenv("TestSessionUniqueID") + ".log";
        environmentVariables.set("logFileName", logFileName);

        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toAbsolutePath().toString() + "\\src\\test\\resources\\";
        System.out.println("Current relative path is: " + path);

        String logFilePath = path + logFileName;
        environmentVariables.set("logFilePath", logFilePath);

        environmentVariables.set("URLsListFilePath", path + JUnitSessionUniqueID + "-urls" + ".log");
        environmentVariables.set("LinkHealthFilePath", path + JUnitSessionUniqueID + "-link-health" + ".log");

        String timeNow = new Timer().getTimeNow();
        String input = "[BEGN] " + "[-] " + "[" + timeNow + "]" + " " + testName + "\n";
        ByteBuffer buffer = ByteBuffer.wrap(input.getBytes());

        File file = new File(logFilePath);

        file.createNewFile();

        FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
        FileLock lock = fileChannel.lock();
        // get an exclusive lock on this channel
        // tryLock( ) is non-blocking. It tries to grab the lock,
        // but if it cannot (when some other process already holds the same lock and it is not shared),
        // it simply returns from the method call. lock( ) blocks until the lock is acquired
        // or the thread that invoked lock( ) is interrupted, or the channel on which the lock( ) method is called is closed.


//        System.out.println("1. Lock acquired: " + lock.isValid());

        fileChannel.write(buffer);

        fileChannel.force(false);
        lock.release();


    }

    @After
    public void check() throws Exception {

        // Construct build-result.properties for slack notification

        environment = System.getenv("environment");

        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toAbsolutePath().toString() + "/";
        String fileName = Config.buildResultPropertiesFile;
        String fullBuildResultsFilePath = path + fileName;

//        GlobalLog.info("Build Properties should be @ " + fullBuildResultsFilePath);
        Config.truncateFile(fullBuildResultsFilePath);
        Config.addProperty(fullBuildResultsFilePath, "project" , "CBS Sports - Media");
        Config.addProperty(fullBuildResultsFilePath, "environment" , environment);

        Config.addProperty(fullBuildResultsFilePath, "icon_emoji" , "eye");

        // Process Eyes Results
        TestResults testResults = eyes.close(false);
        String applitoolsResult = (testResults.isPassed()) ? Config.TestResultPass : Config.TestResultFail;
        String applitoolsURL = testResults.getUrl();
        int noOfSteps = testResults.getSteps();
        int noOfMisMatches = testResults.getMismatches();

        String lastTestCaseRow = System.getenv("lastTestCaseRow");
        String testCaseFound = System.getenv("testCaseFound");

        if ( testCaseFound.equalsIgnoreCase("NONE") ) {
            attachFile(System.getenv("logFilePath"), "Log", testName);
            return;
        }


        int row = Integer.parseInt(lastTestCaseRow) + 1;
        String ageFormula;

        if (noOfSteps > 0) { // zero steps means no visual checks performed

            if (applitoolsResult.equals(Config.TestResultPass)) {
                GlobalLog.info("Applitools Result: " + applitoolsResult);
                ageFormula = SheetsUtils.getAgeFormula("", true); // for the hyperlink without the '='
            } else {
                GlobalLog.error("Applitools Result: " + applitoolsResult);
                GlobalLog.error("Applitools Mismatches: " + noOfMisMatches);
                ageFormula = SheetsUtils.getAgeFormula(noOfMisMatches + " mm ", true); // for the hyperlink without the '='

                Config.addProperty(fullBuildResultsFilePath, "slackNotification" , "1");
                Config.addProperty(fullBuildResultsFilePath, "message" , "We have a problem. See below.");
                Config.addProperty(fullBuildResultsFilePath, "color" , "danger");
                Config.addProperty(fullBuildResultsFilePath, "title" , Config.runTest);
                Config.addProperty(fullBuildResultsFilePath, "title_link" , "https://docs.google.com/spreadsheets/d/" + Config.SheetsID);

            }

            GlobalLog.info("Applitools StepCount: " + noOfSteps);

            String hyperLink = SheetsUtils.constructHyperLink(applitoolsURL, ageFormula, true);
            SheetsUtils.setCellValue(Config.TestStepsWorkSheet, row, Config.getTestStepResultsColumn(), applitoolsResult);
            SheetsUtils.setCellValue(Config.TestStepsWorkSheet, row, Config.getStepAgeColumn(), hyperLink);

        } else {

            if ( testCaseFound != "NOT YET" ) { // not a test driven by sheets

                SheetsUtils.setCellValue(Config.TestStepsWorkSheet, row, Config.getTestStepResultsColumn(), Config.VisualChecks_NOT_PERFORMED);
                SheetsUtils.setCellValue(Config.TestStepsWorkSheet, row, Config.getStepAgeColumn(), "");

            }
        }

        String input = "";

        String timeNow = new Timer().getTimeNow();
        input = input.concat("[ENDS] " + "[-] " + "[" + timeNow + "]" + "\n");
        ByteBuffer buffer = ByteBuffer.wrap(input.getBytes());

        String logFilePath = System.getenv("logFilePath");
        File file = new File(logFilePath);

        FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
        fileChannel.position(fileChannel.size()); // positions at the end of file

        // get an exclusive lock on this channel
        FileLock lock = fileChannel.lock();

//        System.out.println("[End] Lock acquired: " + lock.isValid());

        fileChannel.write(buffer);

        fileChannel.force(false);
        lock.release();

        String testName = System.getenv("TestName");
        attachFile(System.getenv("logFilePath"), "Log", testName);

        // now prepare URLs list file

        input = "";

        if (host.equals("saucelabs")) {
            String sauceSessionURL = Config.sauceSessionHost + sessionID;
            input = "<a href=\"" + sauceSessionURL + "\" target=\"blank\">" + "Sauce Session" + "</a>" + "<br>";

            String sauceField = "<" + sauceSessionURL + "|" + "Sauce Labs" + ">";
            Config.addProperty(fullBuildResultsFilePath, "fieldTitle2" , "Execution Platform");
            Config.addProperty(fullBuildResultsFilePath, "fieldValue2" , sauceField);
        }

        if (applitoolsURL != null) {
            String prepareApplitoolsLink = "<a href=\"" + applitoolsURL + "\" target=\"blank\">" + "Applitools Session" + "</a>" + "<br>";
            input = input.concat(prepareApplitoolsLink);

            String eyesField = "<" + applitoolsURL + "|" + noOfMisMatches + " Mismatches Detected" + ">";
            Config.addProperty(fullBuildResultsFilePath, "fieldTitle1" , "Visual Comparison");
            Config.addProperty(fullBuildResultsFilePath, "fieldValue1" , eyesField);
        }


        buffer = ByteBuffer.wrap(input.getBytes());

        String URLsListFilePath = System.getenv("URLsListFilePath");
        file = new File(URLsListFilePath);

        fileChannel = new RandomAccessFile(file, "rw").getChannel();
        fileChannel.position(fileChannel.size()); // positions at the end of file

        // get an exclusive lock on this channel
        lock = fileChannel.lock();

//        System.out.println("[URL List File] Lock acquired: " + lock.isValid());

        fileChannel.write(buffer);

        fileChannel.force(false);
        lock.release();

        attachHTML(URLsListFilePath, "URLs", testName);

        String whatReallyHappened;

        String expectedToHappen = Config.VISUAL_CHECKS_PASS_MESSAGE;

        if ( applitoolsResult.equals(Config.TestResultPass) ) {

            whatReallyHappened = expectedToHappen;

        } else {

            whatReallyHappened = Config.VISUAL_CHECKS_FAIL_MESSAGE;
            whatReallyHappened = whatReallyHappened + " - " + noOfMisMatches;
        }

        if (applitoolsURL != null) {
            assertThat(whatReallyHappened, is(expectedToHappen));
        }

        if ( Config.checkLinksEnabled.equalsIgnoreCase("yes")) {
            attachFile(System.getenv("LinkHealthFilePath"), "Link Health", testName);
            return;
        }

    }

    @Attachment(value = "{1} for Test {2}", type = "text/csv")
    public String attachFile(String fileFullPath, String description, String testName) throws URISyntaxException, IOException, InterruptedException {

        System.gc();


        File file = new File(fileFullPath);

        FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
        // get an exclusive lock on this channel
        FileLock lock = fileChannel.lock();

        long fileSize = fileChannel.size();

        ByteBuffer buffer = ByteBuffer.allocate((int) fileSize);
        fileChannel.read(buffer);

        buffer.flip();

        String attachmentText = "";

        for (int i = 0; i < fileSize; i++) {

            char character = (char) buffer.get();
            String charToString = String.valueOf(character);
            attachmentText = attachmentText.concat(charToString);

        }

        lock.release();
        fileChannel.close();

        file.setWritable(true);
        file.delete();          // TODO: not happening!


        Log.printConsole("Events Logged into " + fileFullPath + "\n", true);

        return attachmentText;

    }

    @Attachment(value = "{1} for Test {2}", type = "text/html")
    public String attachHTML(String fileFullPath, String description, String testName) throws URISyntaxException, IOException, InterruptedException {

        System.gc();

        Log.printConsole("Trying to attach file: " + fileFullPath, true);

        File file = new File(fileFullPath);

        FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
        // get an exclusive lock on this channel
        FileLock lock = fileChannel.lock();

//        System.out.println("[ATTACHMENT] Lock acquired: " + lock.isValid());
        long fileSize = fileChannel.size();

//        System.out.println("[ATTACHMENT] Size: " + fileSize);
        ByteBuffer buffer = ByteBuffer.allocate((int) fileSize);
        fileChannel.read(buffer);

        buffer.flip();

        String attachmentText = "";

        for (int i = 0; i < fileSize; i++) {

            char character = (char) buffer.get();
            String charToString = String.valueOf(character);
            attachmentText = attachmentText.concat(charToString);

        }

//        System.out.println("[ATTACHMENT TEXT]");
//        System.out.println(attachmentText);
        lock.release();
        fileChannel.close();

        file.setWritable(true);
        file.delete();



        return attachmentText;

    }

    @AfterClass
    public static void postRun() throws IOException {

        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toAbsolutePath().toString() + "\\target\\allure-results\\";
        String fileName = Config.allureEnvironmentFile;
        String fullAllureFilePath = path + fileName;
//        System.out.println("Full File Path to store allure properties is: " + fullFilePath);

        Config.truncateFile(fullAllureFilePath);
        Config.addProperty(fullAllureFilePath, "Sheets.URL" , "https://docs.google.com/spreadsheets/d/" + Config.SheetsID);
        Config.addProperty(fullAllureFilePath, "SauceLabs" , "http://saucelabs.com");
        Config.addProperty(fullAllureFilePath, "Applitools" , "https://eyes.applitools.com");
    }

}
