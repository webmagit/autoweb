package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Kanak on 13/02/2016.
 */
public class DynamicLoading_Page extends Base {

    By startButton = By.cssSelector("#start button");
    By finishText = By.id("finish");

    public DynamicLoading_Page(WebDriver driver) {
        super(driver);
    }

    public void loadExample(String exampleNumber) {
        visit("http://the-internet.herokuapp.com/dynamic_loading/" + exampleNumber);
        click(startButton);
    }

    public Boolean finishTextPresent() {
        Boolean result;

        result = waitForIsDisplayed(finishText, 10);

        return result;

    }
}
