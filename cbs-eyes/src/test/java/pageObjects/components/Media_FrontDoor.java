package pageObjects.components;

import com.applitools.eyes.Eyes;
import org.openqa.selenium.WebDriver;
import pageObjects.Base;
import pageObjects.Implementer;
import ru.yandex.qatools.allure.annotations.Step;
import utilities.GlobalLog;

/**
 * Created by Kanak on 13/02/2016.
 */
public class Media_FrontDoor extends Base {

    public Media_FrontDoor(WebDriver driver) {
        super(driver);
    }

    @Step("Check Front Door")
    public void checkFrontDoor(String object, WebDriver driver, Eyes eyes) {

        GlobalLog.info("at checkFrontDoor");
        Implementer implementer = new Implementer();
        implementer.loadMediaHomePage(driver, eyes);

    }

    public static void waitFor(String object, WebDriver driver, Eyes eyes) throws Exception{

        Thread.sleep(3000);
    }

}
