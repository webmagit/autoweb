package pageObjects.components;

/**
 * Created by kanak.kalburgi on 21/03/2016.
 */

import com.applitools.eyes.Eyes;
import com.applitools.eyes.TestFailedException;
import executionEngine.DriverScriptTest;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import pageObjects.Fantasy;
import utilities.GlobalLog;
import utilities.Log;
import pageObjects.Base;

public class Sample extends Base {

    private Fantasy fantasy;

    public Sample(WebDriver driver) {
        super(driver);
    }

    public void doSomeStuff(String object, WebDriver driver, Eyes eyes) {

        fantasy = new Fantasy();
        GlobalLog.info("new Fantasy instance created");
        fantasy.goTo("", driver);

        try {
            eyes.checkWindow("Fantasy: Home");
            Log.info("Performed eyes.checkWindow");
        } catch (TestFailedException t) {
            Log.printConsole("EYES Exception", true);
            DriverScriptTest.testResult = false;
        }
    }

    public static void click(String object, WebDriver driver, Eyes eyes){
        try {
            Log.info("Clicking on WebElement: " + object);
        } catch (Exception e) {
            DriverScriptTest.testResult = false;
        }
    }

    public void badClick(String object, WebDriver driver, Eyes eyes){

        By fantasyMenu = By.cssSelector("span.logo-sport.bad-element");

        try {
            Log.info("Clicking on WebElement: " + object);
            driver.findElement(fantasyMenu).click();
            throw new NoSuchElementException("");
        } catch (Exception e) {
            DriverScriptTest.testResult = false;
            DriverScriptTest.testResult = false;
        }
    }

    public static void input_Username(String object, WebDriver driver, Eyes eyes){

        try {
            Log.info("Entering Password: " + object);
        } catch (Exception e) {
            DriverScriptTest.testResult = false;
        }
    }

    public static void input_Password(String object, WebDriver driver, Eyes eyes){

        try {
            Log.info("Entering Password: " + object);
        } catch (Exception e) {
            DriverScriptTest.testResult = false;
        }
    }

    public static void waitFor(String object, WebDriver driver, Eyes eyes) throws Exception{

        Thread.sleep(3000);

    }

}
