package pageObjects.components;

import com.applitools.eyes.Eyes;
import org.openqa.selenium.WebDriver;
import pageObjects.Base;
import pageObjects.Implementer;
import ru.yandex.qatools.allure.annotations.Step;
import utilities.GlobalLog;

/**
 * Created by Kanak on 13/02/2016.
 */
public class Fantasy_Home extends Base {

    public Fantasy_Home(WebDriver driver) {
        super(driver);
    }

    @Step("Check Player News")
    public void checkPlayerNews(String object, WebDriver driver, Eyes eyes) {

        GlobalLog.info("at checkPlayerNews");
        Implementer implementer = new Implementer();
        implementer.loadHomePage(driver, eyes);

    }

    public static void waitFor(String object, WebDriver driver, Eyes eyes) throws Exception{

        Thread.sleep(3000);
    }

}
