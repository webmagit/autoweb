package pageObjects.components;

import com.applitools.eyes.Eyes;
import org.openqa.selenium.WebDriver;
import pageObjects.Base;
import pageObjects.Implementer;
import ru.yandex.qatools.allure.annotations.Step;
import utilities.GlobalLog;

/**
 * Created by Kanak on 13/02/2016.
 */
public class Google_AMP extends Base {

    public Google_AMP(WebDriver driver) {
        super(driver);
    }

    @Step("Check Article")
    public void checkArticle(String object, WebDriver driver, Eyes eyes) {

        GlobalLog.info("Checking Article for JS errors");
        Implementer implementer = new Implementer();
        implementer.loadAMPArticle(driver, eyes);

    }

    public static void waitFor(String object, WebDriver driver, Eyes eyes) throws Exception{

        Thread.sleep(3000);
    }

}
