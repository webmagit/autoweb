package pageObjects;

import com.applitools.eyes.Eyes;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import parent.Base;
import ru.yandex.qatools.allure.annotations.Step;
import utilities.GlobalLog;

/**
 * Created by kanak.kalburgi on 9/02/2016.
 */

public class Implementer extends Base {

    By menuSelector = By.cssSelector("span.logo-sport.icon-moon-arena-text-fantasy");
    By cbsLogoDropDown = By.cssSelector(".cbs-logo-dropdown");

    By networkBar = By.cssSelector(".nav-network-bar");
    By siteNav = By.cssSelector(".cbs-site-nav");

    @Step("Fantasy Home")
    public void loadHomePage(WebDriver driver, Eyes eyes) {

        Fantasy fantasy = new Fantasy();

        GlobalLog.info("Fantasy instance available");
        fantasy.goTo("", driver);

        eyes.checkWindow("Fantasy: Home");

        fantasy.clickArena(driver);
        eyes.checkWindow("Click Arena");

        eyes.checkRegion(menuSelector, 4, "aWindowName");
        eyes.checkRegion(cbsLogoDropDown, 4, "Navigation Menu");

        fantasy.hover("FOOTBALL", driver);
        eyes.checkWindow("Hover Arena");

    }

    @Step("Media Front Door")
    public void loadMediaHomePage(WebDriver driver, Eyes eyes) {

        By topMarqueeMain = By.cssSelector("#page-content > div.top-marquee-wrap > div.top-marquee-main");
        By topMarqueeSide = By.cssSelector("#page-content > div.top-marquee-wrap > div.top-marquee-side");
        By topMarqueeWrap = By.cssSelector("#page-content > div.top-marquee-wrap");

        Media media = new Media();

        media.goTo("", driver);

//        eyes.checkWindow("Media: Home");

        eyes.checkRegion(networkBar, 4, "Network Bar");
        eyes.checkRegion(siteNav, 4, "Site Navigation");

//        eyes.checkRegion(topMarqueeMain, 4, "Top Marquee Main");
//        eyes.checkRegion(topMarqueeSide, 4, "Top Marquee Side");
        eyes.checkRegion(topMarqueeWrap, 4, "Top Marquee");
    }

    @Step("Load AMP Article")
    public void loadAMPArticle(WebDriver driver, Eyes eyes) {

        Media media = new Media();

        media.goDirectlyTo("http://www.cbssports.com/article/amp/25105220/#development=1", driver);


        eyes.checkWindow("Article: AMP");


    }
}
