package pageObjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.Log;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by kanak.kalburgi on 11/02/2016.
 */
public class Login_Page extends Base {

    private WebDriver driver;
    By loginFormLocator = By.id("login");
    By usernameLocator = By.id("username");
    By passwordLocator = By.id("password");
    By submitButton = By.cssSelector("button");
    By successMessageLocator = By.cssSelector(".flash.success");
    By failureMessageLocator = By.cssSelector(".flash.error");

    public Login_Page(WebDriver driver) {

        super(driver);
        visit("http://the-internet.herokuapp.com/login");
        Log.info("Here we go. Just opened the heroku app!");
        assertThat(isDisplayed(loginFormLocator), is(true));

    }

    public void with(String username, String password) {

        type(username, usernameLocator);
        type(password, passwordLocator);
        click(submitButton);

        Log.info("Sign-In attempted");

    }

    public Boolean successMessagePresent() {

        Boolean loginStatus;
        loginStatus = isDisplayed(successMessageLocator);

        Log.info("Login? ==> " + loginStatus);
        return loginStatus;

    }

    public Boolean failureMessagePresent() {

        Boolean loginStatus;
        loginStatus = isDisplayed(failureMessageLocator);

        Log.info("Login? ==> " + loginStatus);
        return loginStatus;

    }


}
