package pageObjects;

import com.applitools.eyes.Eyes;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.GlobalLog;

/**
 * Created by Kanak on 13/02/2016.
 */
public class Fantasy extends Base {

    By startButton = By.cssSelector("#start button");
    By finishText = By.id("finish");
    By fantasyMenu = By.cssSelector("span.logo-sport.icon-moon-arena-text-fantasy");

    public Fantasy(WebDriver driver) {
        super(driver);
    }

    public Fantasy() {
        super();
    }

    public void goTo(String section, WebDriver driver) {

        GlobalLog.info("goTo fantasy");
        visit("/fantasy/" + section, true, driver);
    }

    public void hover(String linkText, WebDriver driver) {

        GlobalLog.info("hover fantasy");
        doHover(linkText, driver);
    }

    public void goDirectlyTo(String URL, WebDriver driver) {
        visit(URL, true, driver);
    }

    public Boolean finishTextPresent() {
        Boolean result;

        result = waitForIsDisplayed(finishText, 10);

        return result;

    }

    public void clickArena(WebDriver driver) {

        driver.findElement(fantasyMenu).click();

    }

    public static void waitFor(String object, WebDriver driver, Eyes eyes) throws Exception{

        Thread.sleep(3000);
    }

}
