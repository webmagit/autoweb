package pageObjects;

import com.applitools.eyes.Eyes;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.GlobalLog;

/**
 * Created by Kanak on 13/02/2016.
 */
public class Media extends Base {

    By startButton = By.cssSelector("#start button");
    By finishText = By.id("finish");
    By MediaMenu = By.cssSelector("span.logo-sport.icon-moon-arena-text-fantasy");

    public Media(WebDriver driver) {
        super(driver);
    }

    public Media() {
        super();
    }

    public void goTo(String section, WebDriver driver) {

        GlobalLog.info("goTo Media");
        visit("", true, driver);
    }

    public void hover(String linkText, WebDriver driver) {

        GlobalLog.info("hover fantasy");
        doHover(linkText, driver);
    }

    public void goDirectlyTo(String URL, WebDriver driver) {
        visit(URL, true, driver);
    }

    public Boolean finishTextPresent() {
        Boolean result;

        result = waitForIsDisplayed(finishText, 10);

        return result;

    }

    public void clickArena(WebDriver driver) {

        driver.findElement(MediaMenu).click();

    }

    public static void waitFor(String object, WebDriver driver, Eyes eyes) throws Exception{

        Thread.sleep(3000);
    }

}
