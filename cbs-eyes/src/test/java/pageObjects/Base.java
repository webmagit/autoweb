package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Parameter;
import utilities.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.switchTo;
import static utilities.Crawler.findAllLinks;
import static utilities.Crawler.isLinkBroken;


/**
 * Created by Kanak on 13/02/2016.
 */
public class Base {

//    @Parameter("Scenario")
//    public static String scenario = Config.runTest;

    @Parameter("Base URL")
    public static String baseURL = System.getenv("baseURL");

    @Parameter("Host")
    public static String host = System.getenv("host");

    @Parameter("Browser")
    public static String browser = System.getenv("browser");

    @Parameter("Browser Version")
    public static String browserVersion = System.getenv("browserVersion");

    @Parameter("Platform")
    public static String platform = System.getenv("platform");

    @Parameter("Device")
    public static String device = System.getenv("device");

    private WebDriver driver;

    public Base(WebDriver driver) {

        this.driver = driver;

    }

    public Base() {

    }

    public void visit(String url) {

        if (url.contains("http")) {
            driver.get(url);
        } else {
            driver.get(baseURL + url);
        }
    }

    public void visit(String url, boolean forceHideScrollbars, WebDriver driver) {

//        visit(url);
//        WebDriverRunner.setWebDriver(driver);

        String finalURL = url;

        if (!url.startsWith("http")) {

            finalURL = baseURL + url;

        }

        driver.get(finalURL);
        GlobalLog.info("URL: " + finalURL);

        // capturing javascript errors

        LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
        for (LogEntry entry : logEntries) {
//            GlobalLog.error(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
            GlobalLog.error(entry.getLevel() + " " + entry.getMessage());
        }

        if (forceHideScrollbars) {

            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("document.body.style.overflow = 'hidden'");
        }

        GlobalLog.info("Check Page Links Enabled? " + Config.checkLinksEnabled);

        if ( Config.checkLinksEnabled.equalsIgnoreCase("yes")) {

            GlobalLog.linkHealth("");
            GlobalLog.linkHealth("[" + "ALERT" + "]" + " " + finalURL);
            GlobalLog.linkHealth("");

            Crawler.alertBrokenLinks(finalURL);
            List<WebElement> allLinks = findAllLinks(driver);
            System.out.println("Total Links Detected -- " + allLinks.size());

            for( WebElement element : allLinks){

                try {

                    String link = element.getAttribute("href");
                    String linkStatus = isLinkBroken(new URL(link));

                    if ( !linkStatus.equals("OK") ) {
                        GlobalLog.linkHealth("[" + linkStatus + "]" + " " + link);
                    }

                    //System.out.println("URL: " + element.getAttribute("outerhtml")+ " returned " + isLinkBroken(new URL(element.getAttribute("href"))));

                } catch(Exception exp) {

                    System.out.println("At " + element.getAttribute("innerHTML") + " Exception occurred " + exp.getMessage());

                }

            }

        }
    }



    public WebElement find(By locator) {

        return driver.findElement(locator);

    }

    public void click(By locator) {

        find(locator).click();

    }

    public void doHover(String linkText, WebDriver driver) {

        GlobalLog.info("at base PO hover");

        WebElement element = driver.findElement(By.linkText(linkText));
        Actions action = new Actions(driver);
        action.moveToElement(element).build().perform();

    }

    public void type(String inputText, By locator) {

        find(locator).sendKeys(inputText);

    }

    public Boolean isDisplayed(By locator) {

        try {
            Log.info("****** TRYING TO FIND THE LOCATOR ***** ");
            return find(locator).isDisplayed();
        } catch (org.openqa.selenium.NoSuchElementException exception) {
            Log.info("****** RETURNING FALSE DUE TO NOSUCHELEMENTFOUND EXCEPTION ***** ");
            return false;
        }

    }

    public Boolean waitForIsDisplayed(By locator, Integer... timeout) {

        try {
            waitFor(ExpectedConditions.visibilityOfElementLocated(locator),
                    (timeout.length > 0 ? timeout[0] : null));
        } catch (org.openqa.selenium.TimeoutException exception) {
            return false;
        }
        return true;

    }

    public void waitFor(ExpectedCondition<WebElement> condition, Integer timeout) {
        timeout = timeout != null ? timeout : 5;
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(condition);

    }

    public void assertGameIsWon() {
        $("#gameWin").shouldBe(visible);
    }

    public void assertGameIsLost() {
        $("#gameLost").shouldBe(visible);
    }

    public void switchToWindow(String anotherWindow) {
        switchTo().window(anotherWindow);
    }

    public void shouldBeHidden(String elementName) {
        $(elementName).shouldBe(hidden);
    }

    public void shouldDisappear(String elementName) {
        $(elementName).should(disappear);
    }

    public void waitUntilDisappears(String elementName) {
        $(elementName).waitUntil(disappears, 2000);
    }

    public void shouldNotBeVisible(String elementName) {
        $(elementName).shouldNotBe(visible);
    }

    public void shouldNotPresent(String elementName) {
        $(elementName).shouldNot(present);
    }

    public void shouldNotExist(String elementName) {
        $(elementName).shouldNot(exist);
    }

    public void shouldNotAppear(String elementName) {
        $(elementName).shouldNot(appear);
    }

}
