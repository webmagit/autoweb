package utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Kanak on 27/04/2016.
 */
public class Timer {

    public String getTimeNow() {

        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone(Config.TimeZoneSydney));
        Date today = new Date();
        String timeNow = formatter.format(today);

        return timeNow;

    }
}
