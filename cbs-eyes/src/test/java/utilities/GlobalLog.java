package utilities;

import com.google.common.base.Stopwatch;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

/**
 * Created by kanak.kalburgi on 16/04/2016.
 */
public class GlobalLog {

    static Stopwatch stopWatch = Stopwatch.createStarted();

    public static void writeLog(String logFilePath, String message, boolean append) throws IOException {

        ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());

        File file = new File(logFilePath);

        FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
        fileChannel.position(fileChannel.size()); // positions at the end of file

        // get an exclusive lock on this channel
        FileLock lock = fileChannel.lock();

//        System.out.println("(GlobalLog) Lock acquired: " + lock.isValid());

        fileChannel.write(buffer);

        fileChannel.force(false);
        lock.release();

    }


    public static void info(String message) {

        String logFilePath = System.getenv("logFilePath");

        if ( !stopWatch.isRunning() ) {
            stopWatch = Stopwatch.createStarted();
        }

        try {
            writeLog(logFilePath, "[INFO] " + "[" + stopWatch.elapsed(TimeUnit.SECONDS) +  "]" + " " + message + "\n", true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if ( stopWatch.isRunning() ) {
            stopWatch.reset();
            stopWatch.start();
        }
    }

    public static void error(String message) {

        String logFilePath = System.getenv("logFilePath");
        if ( !stopWatch.isRunning() ) {
            stopWatch = Stopwatch.createStarted();
        }

        try {
            writeLog(logFilePath, "[ERROR] " + "[" + stopWatch.elapsed(TimeUnit.SECONDS) +  "]" + " " + message + "\n", true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if ( stopWatch.isRunning() ) {
            stopWatch.reset();
            stopWatch.start();
        }
    }

    public static void linkHealth(String message) {

        String logFilePath = System.getenv("LinkHealthFilePath");

        try {
            writeLog(logFilePath, message + "\n", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void highLevel(String message) {

        Log.info("A high level message");
    }

}
