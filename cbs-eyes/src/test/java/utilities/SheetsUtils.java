package utilities;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.gdata.client.GoogleAuthTokenFactory;
import com.google.gdata.client.Service;
import com.google.gdata.client.http.HttpGDataRequest;
import com.google.gdata.client.spreadsheet.SpreadsheetQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.Link;
import com.google.gdata.data.spreadsheet.*;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import com.sun.xml.bind.v2.TODO;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.junit.Test;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by kanak.kalburgi on 7/04/2016.
 */
public class SheetsUtils {

    private static final String GOOGLE_ACCESS_TOKEN = "MxEBU0Z9GN2OAhx6yDbtBn9kxkgS7KNgGHhUocClQDg";
    private static final String GOOGLE_REFRESH_TOKEN = "1/074dVizZdfdCOVoAv_MeaJ-qmusMvPa6xPe9_TlWkYM";
    private static final String CLIENT_ID = "1071186678055-ad5alr71l1g190sole1r5msur36nba23.apps.googleusercontent.com";
    private static final String CLIENT_SECRET = "ATgXOJ8qrvLmIWV51sZ9rBs6";

    private static final String GOOGLE_API_HOST = "https://www.googleapis.com/";
    private static final String DEFAULT_SPREADSHEET_QUERY = "https://spreadsheets.google.com/feeds/spreadsheets/private/full";
    private static final String ACCEPT_HEADER_NAME = "Accept";

    private static final String SPREADSHEET_PATH = "https://spreadsheets.google.com/feeds/spreadsheets/private/full/%s";
    private static final String SPREADSHEET_ID = "12hina-Y_5OwfC4mO4uW--NnR7LBn6d-XuoKDUItqrSo"; // for the DataEngine sheet
    private static final String ACCESS_TOKEN_FILE = "accessToken.txt";

    private static String accessToken;
    private static List<WorksheetEntry> worksheets;
    static SpreadsheetService service;
    static List<SpreadsheetEntry> spreadsheets;
    static int sheetPosition;
    static int workSheetPosition;
    static SpreadsheetFeed feed;
    static SpreadsheetQuery q;

    public static void setSheet() throws Exception {

        boolean spreadSheetFound = false;
        boolean workSheetFound = false;


        service = new SpreadsheetService("MySpreadsheetIntegration-v1");
        service.setProtocolVersion(SpreadsheetService.Versions.V1);

        service.setRequestFactory(doAuthorization());

        Log.printConsole("Successful Authorization!", true);


        spreadSheetFound = setSpreadSheetPositionAndStatus(Config.TestSheet);

        if ( !spreadSheetFound ) { // TODO: make an assertion
            Log.error("Test Data Sheet => " + Config.TestSheet + " not found! Exiting now.");
            return;
        }

//        workSheetFound = setWorkSheetPositionAndStatus(Config.TestCasesWorkSheet);
//
//        if ( !workSheetFound ) { // TODO: make an assertion
//            Log.error("Test Work Sheet => " + Config.TestCasesWorkSheet+ " not found! Exiting now.");
//            return;
//        }


    }

    public static boolean setSpreadSheetPositionAndStatus(String targetSpreadSheet) throws Exception {
        boolean spreadSheetFound = false;


        // Define the URL to request.  This should never change.
        URL SPREADSHEET_FEED_URL = new URL("https://spreadsheets.google.com/feeds/spreadsheets/private/full");

        q = new SpreadsheetQuery(new URL(DEFAULT_SPREADSHEET_QUERY));
        try {
            // Make a request to the API and get all spreadsheets.
            feed = service.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
        }
        catch (AuthenticationException e) {
            doRefreshToken(service);
            // Make a request to the API and get all spreadsheets.
            feed = service.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
        }

        spreadsheets = feed.getEntries();

        if (spreadsheets.size() == 0) {
            // TODO: There were no spreadsheets, act accordingly.
        }
        Log.printConsole("Number of Sheets found => " + spreadsheets.size());

        for(sheetPosition = 0; sheetPosition<spreadsheets.size(); sheetPosition++) {
            SpreadsheetEntry spreadsheet = spreadsheets.get(sheetPosition);
            String sheetTitle = spreadsheet.getTitle().getPlainText();
            Log.printConsole("Sheet # " + sheetPosition + " = " + sheetTitle);

            if (sheetTitle.equalsIgnoreCase(Config.TestSheet)) {
                spreadSheetFound = true;
                Log.printConsole("Target Sheet determined at Position # " + sheetPosition, true);
                break;
            }

        }

        return spreadSheetFound;
    }

    public static boolean setWorkSheetPositionAndStatus(String targetWorkSheet) throws IOException, ServiceException {
        // TODO: catch the exception of unsuccessful search here and not return any value back

        boolean workSheetFound = false;

        SpreadsheetEntry spreadsheet = spreadsheets.get(sheetPosition);
        Log.printConsole(spreadsheet.getTitle().getPlainText());

        // Get the first worksheet
        WorksheetFeed worksheetFeed = service.getFeed(
                spreadsheet.getWorksheetFeedUrl(), WorksheetFeed.class);
        worksheets = worksheetFeed.getEntries();

        if (worksheets.size() == 0) {
            // TODO: There were no worksheets, act accordingly.
        }
        Log.printConsole("Number of Worksheets found => " + worksheets.size());

        for(workSheetPosition = 0; workSheetPosition<worksheets.size(); workSheetPosition++) {
            WorksheetEntry worksheet = worksheets.get(workSheetPosition);
            String workSheetTitle = worksheet.getTitle().getPlainText();
            Log.printConsole("rowCount: " + worksheet.getRowCount());
            Log.printConsole("WorkSheet # " + workSheetPosition + " = " + workSheetTitle);

            if (workSheetTitle.equalsIgnoreCase(targetWorkSheet)) {
                workSheetFound = true;
//                Log.printConsole("Target WorkSheet determined at Position # " + workSheetPosition, true);
                break;
            }

        }

        return workSheetFound;
    }

    public static int getCount(String workSheet, int column) throws URISyntaxException, IOException, ServiceException {

       int rowCount = 0;

        setWorkSheetPositionAndStatus(workSheet);
        WorksheetEntry worksheet = worksheets.get(workSheetPosition);

        return worksheet.getRowCount();

        /*
        URL cellFeedUrl = new URI(worksheet.getCellFeedUrl().toString()
                + "?min-row=" +
                Config.TestCaseStartingRow +
                "&min-col=" +
                column +
                "&max-col=" +
                column
        ).toURL();

        CellFeed cellFeed = service.getFeed(cellFeedUrl, CellFeed.class);
        for (CellEntry cell : cellFeed.getEntries()) {
            rowCount++;
        }

        return rowCount;
        */
    }

    public static String getCellValue(String targetSheetName, int row, int col) throws URISyntaxException, IOException, ServiceException {
        int rowCount = 0;

        String cellValue = "";

        setWorkSheetPositionAndStatus(targetSheetName);
        WorksheetEntry worksheet = worksheets.get(workSheetPosition);

        URL cellFeedUrl = new URI(worksheet.getCellFeedUrl().toString()
                + "?min-row="
                + row
                + "&max-row="
                + row
                + "&min-col="
                + col
                + "&max-col="
                + col
        ).toURL();

        CellFeed cellFeed = service.getFeed(cellFeedUrl, CellFeed.class);
        for (CellEntry cell : cellFeed.getEntries()) {
            cellValue = cell.getCell().getInputValue();
        }

        return cellValue;
    }

    public static String setCellValue(String targetSheetName, int row, int col, String value) throws URISyntaxException, IOException, ServiceException {
        int rowCount = 0;

        String cellValue = "";

        setWorkSheetPositionAndStatus(targetSheetName);
        WorksheetEntry worksheet = worksheets.get(workSheetPosition);

        URL cellFeedUrl = new URI(worksheet.getCellFeedUrl().toString()
                + "?min-row="
                + row
                + "&max-row="
                + row
                + "&min-col="
                + col
                + "&max-col="
                + col
        ).toURL(); // min and max don't seem to matter here. We just need the cellFeedUrl
        // to point to the right worksheet

        CellEntry newEntry = new CellEntry(row, col, value);
        service.insert(cellFeedUrl, newEntry);

        return cellValue;
    }

    public static String constructHyperLink(String URL, String displayText, boolean labelContainsFormula) throws URISyntaxException {
        int rowCount = 0;

        String quotes = "\"";

        String quoteOrNoQuote = (labelContainsFormula ? "" : quotes);

        String hyperLink = "=HYPERLINK(" +
                "\"" + //open quotes
                URL +
                "\"" + //close quotes
                "," +
                quoteOrNoQuote +
                displayText +
                quoteOrNoQuote +
                ")";

        return hyperLink;
    }

    private static String getColumnHeader(String workSheetName, int columnPosition) throws ServiceException, IOException, URISyntaxException {


        String columnHeader = getCellValue(workSheetName, 1, columnPosition); // TODO: Header Row assumed 1 by default

        return columnHeader;

    }

    public static int getRowContainingValue(String workSheetName, int col, String searchKey) throws URISyntaxException, IOException, ServiceException {

        String cellValue = "";
        boolean matchFound = false;

        int rowNumber = Config.TestCaseStartingRow;

        setWorkSheetPositionAndStatus(workSheetName);
        WorksheetEntry worksheet = worksheets.get(workSheetPosition);

        URL cellFeedUrl = new URI(worksheet.getCellFeedUrl().toString()
                + "?min-row="
                + rowNumber
                + "&min-col="
                + col
                + "&max-col="
                + col
        ).toURL();

        CellFeed cellFeed = service.getFeed(cellFeedUrl, CellFeed.class);
        for (CellEntry cell : cellFeed.getEntries()) {
            cellValue = cell.getCell().getInputValue();
            if (cellValue.equalsIgnoreCase(searchKey)) {
                matchFound = true;
                rowNumber = cell.getCell().getRow();
                break;
            }
        } //TODO: place unique constraints on the column

        if ( matchFound ) {
            return rowNumber;
        } else {
            return -1; // TODO: find something better to do here (like an assertion)
        }
    }

    public static int getTestStepEndRow(String workSheetName, String testCaseID, int testCaseStart) throws ServiceException, IOException, URISyntaxException {

        int totalNoOfTestSteps = SheetsUtils.getCount(Config.TestStepsWorkSheet, Config.TestStepsColumn);
        Log.printConsole("\n\nTotal steps found : " + totalNoOfTestSteps);

        for (int i = testCaseStart; i <= totalNoOfTestSteps; i++) {
            String searchedTestCaseID = SheetsUtils.getCellValue(Config.TestStepsWorkSheet, i, Config.TestCaseIDColumn);
            Log.printConsole("\n %%Found%% " + searchedTestCaseID + " at row " + i);
            if(!testCaseID.equalsIgnoreCase(searchedTestCaseID)) {
                return i-1;
            }
        }
        return 1;
    }

    public static String getAgeFormula(String preText, boolean isForHyperLink) throws Exception {
        // Remember to have the google spreadsheet in the same timezone as well
        // See File -> Spreadsheet settings

//        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//        formatter.setTimeZone(TimeZone.getTimeZone(Config.TimeZoneSydney));
//        Date today = new Date();
//        String timeNow = formatter.format(today);

        String timeNow = new Timer().getTimeNow();

        String equalsSign = "=";
        String preSign = "";

        if (!isForHyperLink) {
           preSign = equalsSign;
        }

        String age = preSign +
                "CONCAT(\"" +
                preText + "\"" +
                "," +
                "if((ROUNDDOWN(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\"))>1,ROUNDDOWN(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\") & \" days ago\", IF(ROUNDDOWN(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\")>0,ROUNDDOWN(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\") & \" day ago\", IF(HOUR(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\")>1,HOUR(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\") & \" hours ago\",IF(HOUR(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\")>0,HOUR(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\") & \" hour ago\",IF(MINUTE(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\")>0,MINUTE(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\") & \" min ago\",IF(SECOND(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\")>0,SECOND(" +
                Config.TimeNowColumn +
                "-\"" +
                timeNow +
                "\") & \"s ago\",\"mooo\")))))))";

        return age;
    }

    private static Service.GDataRequestFactory doAuthorization() throws IOException {
        Service.GDataRequestFactory requestFactory = new HttpGDataRequest.Factory();

        accessToken = loadAccessToken();

        requestFactory.setAuthToken(new GoogleAuthTokenFactory.OAuth2Token(new GoogleCredential().setAccessToken(accessToken)));

        return requestFactory;
    }

    private static String loadAccessToken() {

        try {
            return new String(Files.readAllBytes(Paths.get(ACCESS_TOKEN_FILE)));
        }
        catch (Exception e)  {
            return GOOGLE_ACCESS_TOKEN;
        }
    }

    private static void doRefreshToken(SpreadsheetService service) throws Exception {
        String accessToken = doGetAccessToken();
        saveAccessToken(accessToken);

        service.getRequestFactory().setAuthToken(new GoogleAuthTokenFactory.OAuth2Token(new GoogleCredential().setAccessToken(accessToken)));
    }

    private static String doGetAccessToken() throws Exception {
        HttpClient client = HttpClients.createDefault();

        String url = String.format(
                "%soauth2/v3/token?client_id=%s&client_secret=%s&refresh_token=%s&grant_type=refresh_token",
                GOOGLE_API_HOST,
                CLIENT_ID,
                CLIENT_SECRET,
                GOOGLE_REFRESH_TOKEN
        );
        HttpPost post = new HttpPost(url);
        post.addHeader(ACCEPT_HEADER_NAME, "application/x-www-form-urlencoded");

        try {
            HttpResponse response = client.execute(post);

            JSONObject object = readJson(response);

            return object.getString("access_token");
        }
        finally {
            post.releaseConnection();
        }
    }

    private static void saveAccessToken(String accessToken) {

        String fileName = ACCESS_TOKEN_FILE;

        try {

            /*
            Path currentRelativePath = Paths.get("");
            String s = currentRelativePath.toAbsolutePath().toString();
            Log.printConsole("Current relative path is: " + s);
            */

            FileWriter fileWriter = new FileWriter(fileName);

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(accessToken);

            bufferedWriter.close();

        } catch(IOException ex) {

            ex.printStackTrace();
        }
    }

    private static JSONObject readJson(HttpResponse response) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }

        return new JSONObject(result.toString());
    }
}
