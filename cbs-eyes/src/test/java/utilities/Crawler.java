package utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kanak.kalburgi on 6/05/2016.
 */
public class Crawler {

    @Step
    public static void alertBrokenLinks(String URL) {

        GlobalLog.info("I'm checking for broken links @ " + URL);

    }

    public static List findAllLinks(WebDriver driver){

        List <WebElement> elementList = driver.findElements(By.tagName("a"));

        elementList.addAll(driver.findElements(By.tagName("img")));

        List finalList = new ArrayList(); ;

        for (WebElement element : elementList) {

            if(element.getAttribute("href") != null) {

                finalList.add(element);

            }

        }

        return finalList;

    }

    public static String isLinkBroken(URL url) throws Exception {

        String response = "";

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {

            connection.connect();
            response = connection.getResponseMessage();
            connection.disconnect();

            return response;

        } catch(Exception exp) {

            return exp.getMessage();

        }

    }
}
