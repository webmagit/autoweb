package utilities;

import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Properties;

/**
 * Created by Kanak on 13/02/2016.
 */
public class Config {

    public static final String sauceUser = System.getProperty("SAUCE_USERNAME", "kanakj9");

    public static final String sauceKey = System.getProperty("SAUCE_ACCESS_KEY", "edefacd4-f8c3-4de8-9d22-effc163b7479");
    public static final String sauceHost = System.getProperty("SAUCE_HOST", "ondemand.saucelabs.com");
    public static final String saucePort = System.getProperty("SAUCE_PORT", "80");
    public static final String sauceHub = System.getProperty("SAUCE_HUB", "wd/hub");
    public static final String sauceSessionHost = "http://saucelabs.com/beta/tests/";

    public static final String eyesEnabled = System.getProperty("EYES_ENABLED", "YES");
    public static final String applitoolsKey = System.getProperty("APPLITOOLS_API_KEY", "1WmH0YihOwJaH0MZTZlrD5Itq3dM0caqwmcYI2FG1fY110");

    public static final String runTest = System.getProperty("runTest", "TestCase001");
    public static final String checkLinksEnabled = System.getProperty("checkLinks", "no");


    public static final String DESKTOP = "Desktop";
    public static final String TABLET = "Tablet";
    public static final String MOBILE = "Mobile";

    public static final String WINDOWS_10 = "Windows 10";
    public static final String MACOS_10 = "OS X 10.10";
    public static final String WINDOWS_7 = "Windows 7";
    public static final String IOS_9 = "iOS 9.1";
    public static final String ANDROID_5 = "Android 5.1";

    public static final String CHROME = "Chrome";
    public static final String FIREFOX = "Firefox";
    public static final String SAFARI = "Safari";
    public static final String EDGE = "Microsoft Edge";
    public static final String IE = "Internet Explorer";

    public static final int pageWidth = 1319;
    public static final int pageHeight = 613;

    public static final String demoTestSheet = "DataEngine";
    public static final String demoTestWorkSheet = "DEMO";

    public static final String TestSheet = System.getProperty("TEST_SPREAD_SHEET", "DataEngine");
    public static final String TestCasesWorkSheet = System.getProperty("TEST_CASES_WORK_SHEET", "Test Cases");
    public static final String TestStepsWorkSheet = System.getProperty("TEST_STEPS_WORK_SHEET", "QA");
    public static final String SheetsID = "12hina-Y_5OwfC4mO4uW--NnR7LBn6d-XuoKDUItqrSo"; // not directly useful.
    // The SheetsID above is just for showing the URL on reports.

    // Test Case Sheet configuration
    public static final int TestCaseIDColumn = 1;
    public static final int TestRunModeColumn = 3;
    public static final int TestCaseResultColumn = 4;
    public static final int TestCaseHeaderRow = 1;
    public static final int TestCaseStartingRow = 2;
    public static final String RunModeOn = "ON";
    public static final String RunModeOff = "OFF";
    public static final String TestExecutionInProgress = "STARTED ";
    public static final String TestResultPass = "PASS";
    public static final String TestResultFail = "FAIL";

    // Test Steps Sheet configuration
    public static final int TestStepsColumn = 2;
    public static final int TestStepDescription = 3;
    public static final int TestStepConditionColumn = 6;
    public static final int TestStepPageObjectColumn = 4;
    public static final int TestStepActionKeywordColumn = 5;
    public static int TestStepResultsColumn;
    public static final String TestStep_Keyword_SKIP = "SKIP";
    public static final String TestStep_Keyword_PASS = "PASS";
    public static final String TestStep_Keyword_FAIL = "FAIL";
    public static final String TestStep_Keyword_WONT_EXECUTE = "WONT EXECUTE";
    public static final String VisualChecks_NOT_PERFORMED = "NO CHECKS";
    public static final String TestStep_Keyword_OPTIONAL = "OPTIONAL";
    public static final String TestStep_Keyword_IN_PROGRESS = "IN PROGRESS";
    public static final String TimeNowColumn = "'SETTINGS'!C1";

    public static final String TimeZoneSydney = "GMT+10";
    // GMT+10 == Sydney TimeZone // TODO: Is GMT+10 valid despite changes to Daylight Savings?

    public static final boolean PrintConsole = false;
    public static final String allureEnvironmentFile = "environment.properties";
    public static final String buildResultPropertiesFile = "build-result.properties";
    public static final String PageObjectsPackage = "pageObjects.components";

    public static final String VISUAL_CHECKS_PASS_MESSAGE = "Visual Checks - PASSED";
    public static final String VISUAL_CHECKS_FAIL_MESSAGE = "Visual Checks - FAILED";

    public static int getTestStepResultsColumn() {

        String browser = System.getenv("browser");
        String browserVersion = System.getenv("browserVersion");
        String device = System.getenv("device");
        String platform = System.getenv("platform");

        if (device.equalsIgnoreCase(DESKTOP) && platform.equals(MACOS_10) && browser.equalsIgnoreCase(CHROME) && browserVersion.equalsIgnoreCase(browserVersion)) {

            TestStepResultsColumn = 7;

        } else if (device.equalsIgnoreCase(DESKTOP) && platform.equals(MACOS_10) && browser.equalsIgnoreCase(FIREFOX) && browserVersion.equalsIgnoreCase(browserVersion)) {

            TestStepResultsColumn = 9;

        } else if (device.equalsIgnoreCase(DESKTOP) && platform.equals(MACOS_10) && browser.equalsIgnoreCase(SAFARI) && browserVersion.equalsIgnoreCase(browserVersion)) {

            TestStepResultsColumn = 11;

        } else if (device.equalsIgnoreCase(DESKTOP) && platform.equals(WINDOWS_10) && browser.equalsIgnoreCase(EDGE) && browserVersion.equalsIgnoreCase(browserVersion)) {

            TestStepResultsColumn = 13; // watch out!

        } else if (device.equalsIgnoreCase(DESKTOP) && platform.equals(WINDOWS_7) && browser.equalsIgnoreCase(IE) && browserVersion.equalsIgnoreCase(browserVersion)) {

            TestStepResultsColumn = 15; // this one.

        } else if (device.equalsIgnoreCase(DESKTOP) && platform.equals(WINDOWS_7) && browser.equalsIgnoreCase(IE) && browserVersion.equalsIgnoreCase(browserVersion)) {

            TestStepResultsColumn = 17;

        } else if (device.equalsIgnoreCase(TABLET) && platform.equals(IOS_9) && browser.equalsIgnoreCase(SAFARI) && browserVersion.equalsIgnoreCase(browserVersion)) {

            TestStepResultsColumn = 19;

        } else if (device.equalsIgnoreCase(TABLET) && platform.equals(ANDROID_5) && browser.equalsIgnoreCase(SAFARI) && browserVersion.equalsIgnoreCase(browserVersion)) {

            TestStepResultsColumn = 21;

        } else {

            TestStepResultsColumn = 23;
//            GlobalLog.error("Couldn't determine Platform Configuration");
        }

        return TestStepResultsColumn;
    }

    public static int getStepAgeColumn() {
        return TestStepResultsColumn + 1;
    }

    public static final String randomID = java.util.UUID.randomUUID().toString();

    public static void addProperty(String fullFilePath, String propertyName, String propertyValue) throws IOException {

        String message = propertyName + "=" + propertyValue + "\n";

        ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());

        File file = new File(fullFilePath);

        FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
        fileChannel.position(fileChannel.size()); // positions at the end of file

        // get an exclusive lock on this channel
        FileLock lock = fileChannel.lock();

//        System.out.println("(addProperty) Lock acquired: " + lock.isValid());

        fileChannel.write(buffer);

        fileChannel.force(false);
        lock.release();
    }

    public static void truncateFile(String fullFilePath) throws IOException {

        File file = new File(fullFilePath);

        FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();
        fileChannel.position(fileChannel.size()); // positions at the end of file

        // get an exclusive lock on this channel
        FileLock lock = fileChannel.lock();

//        System.out.println("(truncateFile) Lock acquired: " + lock.isValid());

        fileChannel.truncate(0);

        fileChannel.force(false);
        lock.release();
    }

    public static void setPlatformConfiguration() throws IOException {

        // see cbs-eyes\src\main\resources\filtered_resource.properties

        EnvironmentVariables environmentVariables = new EnvironmentVariables();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream stream = classLoader.getResourceAsStream("filtered_resource.properties");

        Properties properties = new Properties();
        properties.load(stream);

        environmentVariables.set("environment", properties.getProperty("environment", "not_filtered"));
        environmentVariables.set("browser", properties.getProperty("browser", "not_filtered"));
        environmentVariables.set("screenResolution", properties.getProperty("screenResolution", "not_filtered"));
        environmentVariables.set("baseURL", properties.getProperty("baseURL", "not_filtered"));
        environmentVariables.set("host", properties.getProperty("host", "not_filtered"));
        environmentVariables.set("browserVersion", properties.getProperty("browserVersion", "not_filtered"));
        environmentVariables.set("device", properties.getProperty("device", "not_filtered"));
        environmentVariables.set("platform", properties.getProperty("platform", "not_filtered"));
        environmentVariables.set("activeProfile", properties.getProperty("activeProfile", "not_filtered"));

    }
}
