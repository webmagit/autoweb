package tests.samples;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import pageObjects.DynamicLoading_Page;
import parent.Base;
import tests.groups.Deep;
import utilities.Log;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by kanak.kalburgi on 9/02/2016.
 */

@Category(Deep.class)
public class DynamicLoadingPageTest extends Base {

    private DynamicLoading_Page dynamicLoading;

    @Before
    public void testSetup() {

        dynamicLoading = new DynamicLoading_Page(driver);
        Log.info("Test Setup Success. Now starting our Tests.");

    }

    @Test
    public void hiddenElementLoads() {

        dynamicLoading.loadExample("1");
        assertThat(dynamicLoading.finishTextPresent(), is(true));

    }

    @Test
    public void elementAppears() {

        dynamicLoading.loadExample("2");
        assertThat(dynamicLoading.finishTextPresent(), is(true));

    }

}
