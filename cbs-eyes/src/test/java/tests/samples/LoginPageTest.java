package tests.samples;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import pageObjects.Login_Page;
import parent.Base;
import tests.groups.Shallow;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by kanak.kalburgi on 9/02/2016.
 */

public class LoginPageTest extends Base {

    private Login_Page loginPage;

    @Before
    public void testSetup() {

        loginPage = new Login_Page(driver);
    }

    @Test
    @Category(Shallow.class)
    public void loginSuccessCheck() {

        loginPage.with("tomsmith", "SuperSecretPassword!");
        assertThat(loginPage.successMessagePresent(), is(true));

    }

    @Test
    @Category(Shallow.class)
    public void loginFailureCheck() {

        loginPage.with("tomsmith", "DamnDumbPassword!");
        assertThat(loginPage.failureMessagePresent(), is(true));
        assertThat(loginPage.successMessagePresent(), is(false));


    }

}