package tests.samples;

/**
 * Created by kanak.kalburgi on 20/03/2016.
 */

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pageObjects.mod.Home_Page;
import pageObjects.mod.LogIn_Page;

import java.util.concurrent.TimeUnit;

public class PageObjectModelTest {

    private static WebDriver driver = null;

    @Test
    public void pomSample() {

        driver = new FirefoxDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("http://www.store.demoqa.com");

        // Use Page Object library now

        Home_Page.lnk_MyAccount(driver).click();

        LogIn_Page.txtbx_UserName(driver).sendKeys("unrealuser");

        LogIn_Page.txtbx_Password(driver).sendKeys("pausebird");

//        LogIn_Page.btn_LogIn(driver).click();

//        System.out.println(" Login Successfully, now it is the time to Log Off buddy.");

//        Home_Page.lnk_LogOut(driver).click();

        driver.quit();

        }

}
