package tests.samples;

import org.junit.Test;
import parent.Base;

/**
 * Created by kanak.kalburgi on 9/02/2016.
 */

public class ParallelTest extends Base {

    @Test
    public void one() throws InterruptedException {
        Thread.sleep(1000 * 5);
    }

    @Test
    public void two() throws InterruptedException {
        Thread.sleep(1000 * 5);
    }

    @Test
    public void three() throws InterruptedException {
        Thread.sleep(1000 * 5);
    }

    @Test
    public void four() throws InterruptedException {
        Thread.sleep(1000 * 5);
    }

    @Test
    public void five() throws InterruptedException {
        Thread.sleep(1000 * 5);
    }

}