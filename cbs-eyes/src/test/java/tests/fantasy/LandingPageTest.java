package tests.fantasy;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;
import pageObjects.Fantasy;
import parent.Base;
import tests.groups.Driver;
import utilities.GlobalLog;
import utilities.Log;

/**
 * Created by kanak.kalburgi on 9/02/2016.
 */

@Category(Driver.class)
public class LandingPageTest extends Base {

    private Fantasy fantasy;

    By menuSelector = By.cssSelector("span.logo-sport.icon-moon-arena-text-fantasy");
    By cbsLogoDropDown = By.cssSelector(".cbs-logo-dropdown");

    @Before
    public void testSetup() {

        fantasy = new Fantasy();
        Log.info("Test Setup Success. Now starting our Tests.");

    }

    @Test
    public void loadHomePage() {

        GlobalLog.info("new Fantasy instance created");
        fantasy.goTo("", driver);
        eyes.checkWindow("Fantasy: Home");

        fantasy.clickArena(driver);
        eyes.checkWindow("Click Arena");

        eyes.checkRegion(menuSelector, 4, "aWindowName");
        eyes.checkRegion(cbsLogoDropDown, 4, "Navigation Menu");

        fantasy.hover("FOOTBALL", driver);
        eyes.checkWindow("Hover Arena");

        eyes.close();

    }


}
