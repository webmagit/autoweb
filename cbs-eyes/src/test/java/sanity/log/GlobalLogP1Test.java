package sanity.log;

import org.junit.*;
import org.junit.experimental.categories.Category;
import parent.Base;
import ru.yandex.qatools.allure.annotations.Attachment;
import tests.groups.*;
import utilities.Config;
import utilities.GlobalLog;
import utilities.Log;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.fail;

/**
 * Created by kanak.kalburgi on 9/02/2016.
 */

@Category({Sanity.class, GlobalLogging.class})
public class GlobalLogP1Test extends Base {


    @BeforeClass
    public static void setUp() {

//        GlobalLog.startTest("my test");
        Log.info("1 NA => " + System.getenv("TestName") + " Logging to File: " + System.getenv("TestSessionUniqueID"));

    }

    @Before
    public void testBefore() {

//        GlobalLog.startTest("my test");
        Log.info("2 BEFORE => " + System.getenv("TestName") + " Logging to File: " + System.getenv("TestSessionUniqueID"));

    }

    @After
    public void testAfter() throws IOException, URISyntaxException {

//        Log.startTestCase("After Method");
//        attachFile("log.log");
        Log.info("4 AFTER => " + System.getenv("TestName") + " Logging to File: " + System.getenv("TestSessionUniqueID"));

    }


    @Test
    public void successCheck() {

//        Log.info("3 @Test => " + System.getenv("TestName") + " Logging to File: " + System.getenv("TestSessionUniqueID"));

        GlobalLog.info("Info from successCheck (P1)");
        GlobalLog.info("successCheck doing GREAT (P1).");
    }



    @AfterClass
    public static void tearDown() {
//        GlobalLog.endTest("my test");
        Log.info("5 NA => " + System.getenv("TestName") + " Logging to File: " + System.getenv("TestSessionUniqueID"));

    }
}
