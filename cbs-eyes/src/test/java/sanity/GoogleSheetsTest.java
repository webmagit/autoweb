package sanity;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.gdata.client.GoogleAuthTokenFactory;
import com.google.gdata.client.Service;
import com.google.gdata.client.http.HttpGDataRequest;
import com.google.gdata.client.spreadsheet.SpreadsheetQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.*;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import parent.Base;
import tests.groups.Sanity;
import utilities.Config;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Category(Sanity.class)
public class GoogleSheetsTest extends Base {

    private static final String GOOGLE_ACCESS_TOKEN = "MxEBU0Z9GN2OAhx6yDbtBn9kxkgS7KNgGHhUocClQDg";
    private static final String GOOGLE_REFRESH_TOKEN = "1/074dVizZdfdCOVoAv_MeaJ-qmusMvPa6xPe9_TlWkYM";
    private static final String CLIENT_ID = "1071186678055-ad5alr71l1g190sole1r5msur36nba23.apps.googleusercontent.com";
    private static final String CLIENT_SECRET = "ATgXOJ8qrvLmIWV51sZ9rBs6";

    private static final String GOOGLE_API_HOST = "https://www.googleapis.com/";
    private static final String DEFAULT_SPREADSHEET_QUERY = "https://spreadsheets.google.com/feeds/spreadsheets/private/full";
    private static final String ACCEPT_HEADER_NAME = "Accept";

    private static final String SPREADSHEET_PATH = "https://spreadsheets.google.com/feeds/spreadsheets/private/full/%s";
    private static final String SPREADSHEET_ID = Config.SheetsID; // for the DataEngine sheet
    private static final String ACCESS_TOKEN_FILE = "resources/accessToken.txt";

    private String accessToken;

    @Test
    public void googleSheetsIntegration() throws Exception {

        int sheetPosition;
        boolean sheetFound = false;
        boolean workSheetFound = false;
        // Define the URL to request.  This should never change.
        URL SPREADSHEET_FEED_URL = new URL("https://spreadsheets.google.com/feeds/spreadsheets/private/full");


        SpreadsheetService service = new SpreadsheetService("MySpreadsheetIntegration-v1");
        service.setProtocolVersion(SpreadsheetService.Versions.V1);

        service.setRequestFactory(doAuthorization());

        System.out.println("Successful Authorization!");

        SpreadsheetQuery q = new SpreadsheetQuery(new URL(DEFAULT_SPREADSHEET_QUERY));
        SpreadsheetFeed feed;
        try {
            // Make a request to the API and get all spreadsheets.
            feed = service.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
        }
        catch (AuthenticationException e) {
            doRefreshToken(service);
            // Make a request to the API and get all spreadsheets.
            feed = service.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
        }

        List<SpreadsheetEntry> spreadsheets = feed.getEntries();

        if (spreadsheets.size() == 0) {
            // TODO: There were no spreadsheets, act accordingly.
        }
        System.out.println("Number of Sheets found => " + spreadsheets.size());

        for(sheetPosition = 0; sheetPosition<spreadsheets.size(); sheetPosition++) {
            SpreadsheetEntry spreadsheet = spreadsheets.get(sheetPosition);
            String sheetTitle = spreadsheet.getTitle().getPlainText();
            System.out.println("Sheet # " + sheetPosition + " = " + sheetTitle);

            if (sheetTitle.equalsIgnoreCase(Config.demoTestSheet)) {
                sheetFound = true;
                System.out.println("Target Sheet determined at Position # " + sheetPosition);
                break;
            }

        }

        if ( !sheetFound ) { // TODO: make an assertion
            System.out.println("Sample Data Sheet => " + Config.demoTestSheet + " not found! Exiting now.");
            return;
        }

        SpreadsheetEntry spreadsheet = spreadsheets.get(sheetPosition);
        System.out.println(spreadsheet.getTitle().getPlainText());

        // Get the first worksheet
        WorksheetFeed worksheetFeed = service.getFeed(
                spreadsheet.getWorksheetFeedUrl(), WorksheetFeed.class);
        List<WorksheetEntry> worksheets = worksheetFeed.getEntries();

        if (worksheets.size() == 0) {
            // TODO: There were no worksheets, act accordingly.
        }
        System.out.println("Number of Worksheets found => " + worksheets.size());

        for(sheetPosition = 0; sheetPosition<worksheets.size(); sheetPosition++) {
            WorksheetEntry worksheet = worksheets.get(sheetPosition);
            String workSheetTitle = worksheet.getTitle().getPlainText();
            System.out.println("WorkSheet # " + sheetPosition + " = " + workSheetTitle);

            if (workSheetTitle.equalsIgnoreCase(Config.demoTestWorkSheet)) {
                workSheetFound = true;
                System.out.println("Target WorkSheet determined at Position # " + sheetPosition);
                break;
            }

        }


        if ( !workSheetFound ) { // TODO: make an assertion
            System.out.println("Sample Work Sheet => " + Config.demoTestWorkSheet + " not found! Exiting now.");
            return;
        }

        WorksheetEntry worksheet = worksheets.get(sheetPosition);

        int startingRow = 2;
        int stopRow;
        int startingColumn = 2;
        int endingColumn = 2;

        // Fetch column 4, and every row after row 1.
        URL cellFeedUrl = new URI(worksheet.getCellFeedUrl().toString()
                + "?min-row=" +
                startingRow +
                "&min-col=" +
                startingColumn +
                "&max-col=" +
                endingColumn
        ).toURL();

        CellEntry newEntry = new CellEntry(6, 6, "FIZZ");
        service.insert(cellFeedUrl, newEntry);

        CellFeed cellFeed = service.getFeed(cellFeedUrl, CellFeed.class);
        String fillUp = "OK";
        // Iterate through each cell, printing its value.
        for (CellEntry cell : cellFeed.getEntries()) {

            int currentRow = cell.getCell().getRow();

            // Print the cell's address in A1 notation
            System.out.print(cell.getTitle().getPlainText() + "\t");
            System.out.print(cell.getCell().getCol() + "<---");
            System.out.print(cell.getCell().getRow() + "$---");
            // Print the cell's address in R1C1 notation
            System.out.print(cell.getId().substring(cell.getId().lastIndexOf('/') + 1) + "\t");
            // Print the cell's formula or text value
            System.out.print(cell.getCell().getInputValue() + "\t");
            cell.changeInputValueLocal(fillUp);
            cell.update();

            // Print the cell's calculated value if the cell's value is numeric
            // Prints empty string if cell's value is not numeric
//            System.out.print(cell.getCell().getNumericValue() + "\t");
            // Print the cell's displayed value (useful if the cell has a formula)
//            System.out.println(cell.getCell().getValue() + "\t");


            // Fetch the list feed of the worksheet.
            URL listFeedUrl = worksheet.getListFeedUrl();
            ListFeed listFeed = service.getFeed(listFeedUrl, ListFeed.class);
            ListEntry row = listFeed.getEntries().get(currentRow-startingRow);
            row.getCustomElements().setValueLocal("Clear", "YES");
            row.update();
        }

    }


//    @Test
    public void getAccessToken() throws Exception {
        String accessToken = doGetAccessToken();
        System.out.println("Retrieved Access Token: " + accessToken);

        Assert.assertNotNull(accessToken);
    }


    private void doRefreshToken(SpreadsheetService service) throws Exception {
        String accessToken = doGetAccessToken();
        saveAccessToken(accessToken);

        service.getRequestFactory().setAuthToken(new GoogleAuthTokenFactory.OAuth2Token(new GoogleCredential().setAccessToken(accessToken)));
    }

    private String doGetAccessToken() throws Exception {
        HttpClient client = HttpClients.createDefault();

        String url = String.format(
            "%soauth2/v3/token?client_id=%s&client_secret=%s&refresh_token=%s&grant_type=refresh_token",
            GOOGLE_API_HOST,
            CLIENT_ID,
            CLIENT_SECRET,
            GOOGLE_REFRESH_TOKEN
        );
        HttpPost post = new HttpPost(url);
        post.addHeader(ACCEPT_HEADER_NAME, "application/x-www-form-urlencoded");

        try {
            HttpResponse response = client.execute(post);

            JSONObject object = readJson(response);

            return object.getString("access_token");
        }
        finally {
            post.releaseConnection();
        }
    }

    private Service.GDataRequestFactory doAuthorization() throws IOException {
        Service.GDataRequestFactory requestFactory = new HttpGDataRequest.Factory();

        accessToken = loadAccessToken();

        requestFactory.setAuthToken(new GoogleAuthTokenFactory.OAuth2Token(new GoogleCredential().setAccessToken(accessToken)));

        return requestFactory;
    }

    private SpreadsheetEntry findSpreadSheet(SpreadsheetFeed feed) throws Exception {
        for(int index = 0; index < feed.getEntries().size(); index++) {
            SpreadsheetEntry spreadSheet = feed.getEntries().get(index);

            if(spreadSheet.getId().equals(String.format(SPREADSHEET_PATH, SPREADSHEET_ID))) {
                return spreadSheet;
            }
        }

        return null;
    }

    private JSONObject readJson(HttpResponse response) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }

        return new JSONObject(result.toString());
    }

    private JSONArray readJsonArray(HttpResponse response) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }

        return new JSONArray(result.toString());
    }
    
    private void saveAccessToken(String accessToken) {
        this.accessToken = accessToken;

        String fileName = ACCESS_TOKEN_FILE;

        try {

            Path currentRelativePath = Paths.get("");
            String s = currentRelativePath.toAbsolutePath().toString();
            System.out.println("Current relative path is: " + s);

            FileWriter fileWriter = new FileWriter(fileName);

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(accessToken);

            bufferedWriter.close();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    private String loadAccessToken() {
        try {
            return new String(Files.readAllBytes(Paths.get(ACCESS_TOKEN_FILE)));
        }
        catch (Exception e)  {
            return GOOGLE_ACCESS_TOKEN;
        }
    }
}
