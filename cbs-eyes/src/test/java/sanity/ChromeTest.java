package sanity;

/**
 * Created by kanak.kalburgi on 4/04/2016.
 */


import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import parent.Base;
import tests.groups.Sanity;

//@Category(Sanity.class)
public class ChromeTest extends Base {

    @Test
    public void startWebDriver(){

        System.setProperty("webdriver.chrome.driver",
                System.getProperty("user.dir") + "/vendor/chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.navigate().to("http://google.com");

        Assert.assertTrue("Title should start differently",
                driver.getTitle().startsWith("Google"));

        driver.close();
        driver.quit();
    }
}

