package sanity;

import org.junit.*;
import org.junit.experimental.categories.Category;
import parent.Base;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Step;
import tests.groups.ProfileChecks;
import tests.groups.Sanity;
import utilities.Config;
import utilities.GlobalLog;
import utilities.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by kanak.kalburgi on 9/02/2016.
 */

@Category({ProfileChecks.class})
public class ProfileActivationTest extends Base {

    @Test
    public void successCheck() throws IOException {

        Config.setPlatformConfiguration();

        GlobalLog.info("Profile Activation Test");
        GlobalLog.info("BaseURL: " + System.getenv("baseURL"));
        GlobalLog.info("Platform: " + System.getenv("platform"));
        GlobalLog.info("Device: " + System.getenv("device"));
        GlobalLog.info("Browser: " + System.getenv("browser"));
        GlobalLog.info("BrowserVersion: " + System.getenv("browserVersion"));
        GlobalLog.info("Host: " + System.getenv("host"));

    }

    @Step
    public void checkThat2is2() {
        assertThat(2, is(2));
    }

    @Features("A Feature")
    public void aFeature() {
        assertThat(4, is(4));
    }

}
