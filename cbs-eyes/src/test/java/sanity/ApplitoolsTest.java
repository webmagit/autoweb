package sanity;

import com.applitools.eyes.Eyes;
import com.applitools.eyes.RectangleSize;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import tests.groups.Sanity;

import java.net.URISyntaxException;

import static utilities.Config.*;

//@Category(Sanity.class)
public class ApplitoolsTest {

    @Test
    public void connectsWell() throws URISyntaxException, InterruptedException {

        WebDriver driver = new FirefoxDriver();

        Eyes eyes = new Eyes();
        eyes.setApiKey(System.getProperty("APPLITOOLS_API_KEY", applitoolsKey));

        try {

            // Start visual testing with browser viewport set to 1024x768.
            // Make sure to use the returned driver from this point on.
            driver = eyes.open(driver, "Applitools", "Test Web Page", new RectangleSize(1024, 768));

            driver.get("http://applitools.com");

            // Visual validation point #1
            eyes.checkWindow("Main Page");

            // End visual testing. Validate visual correctness.
            eyes.close();

        } finally {

            // Abort test in case of an unexpected error.
            eyes.abortIfNotClosed();
            driver.close();

        }
    }
}
