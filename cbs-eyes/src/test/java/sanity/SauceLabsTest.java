package sanity;

import com.saucelabs.saucerest.SauceREST;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import tests.groups.Sanity;
import utilities.Config;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static utilities.Config.*;


/**
 * Created by kanak.kalburgi on 5/04/2016.
 */

@Category(Sanity.class)
public class SauceLabsTest {

    protected WebDriver driver;
    private String testName = "SauceSanityTest";
    private String sessionId;
    private SauceREST sauceClient;

    @Test
    public void connectsWell() throws IOException {


        Config.setPlatformConfiguration();
        String browser = System.getenv("browser");
        String browserVersion = System.getenv("browserVersion");
        String platform = System.getenv("platform");


        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("browserName", browser);
        capabilities.setCapability("version", browserVersion);
        capabilities.setCapability("platform", platform);
        capabilities.setCapability("name", testName);
        String sauceUrl = String.format(
                "http://%s:%s@%s:%s/%s",
                sauceUser, sauceKey, sauceHost, saucePort, sauceHub);

        driver = new RemoteWebDriver(new URL(sauceUrl), capabilities);
        sessionId = ((RemoteWebDriver) driver).getSessionId().toString();
        sauceClient = new SauceREST(sauceUser, sauceKey);
        sauceClient.jobPassed(sessionId);

        driver.get("http://google.com");
        driver.quit();

    }

}
