package sanity;

import com.google.common.io.Files;
import com.sun.jna.platform.unix.X11;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Attachment;
import tests.groups.Debug;
import parent.Base;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.open;

import com.codeborne.selenide.Screenshots;
import tests.groups.Sanity;
import utilities.GlobalLog;

/**
 * Created by kanak.kalburgi on 27/04/2016.
 */
//@Category(Sanity.class)
public class SelenideTest extends Base {

    @Test
    public void googleSearch() throws IOException, InterruptedException {
        open("http://google.com");
        $(By.name("q")).val("selenide").pressEnter();
        assertThat(title(), is("Google"));
        $$("#ires .g").shouldHave(size(10));
        $("#ires .g").shouldHave(text("selenide.org"));
        $(By.linkText("Next")).click();
        $$("#ires .g").shouldHave(size(10));
        $("#ires .srg").shouldHave(text("https://twitter.com/jselenide"));
        screenshot(title());
    }

//    @After
    public void tearDown() throws IOException {
        screenshot(title());
    }

    @Attachment(value = "Screenshot via Selenide - {0}", type = "image/png")
    public byte[] screenshot(String pageTitle) throws IOException {
        File screenshot = Screenshots.takeScreenShotAsFile();
        return Files.toByteArray(screenshot);
    }


}
