package sanity;

/**
 * Created by kanak.kalburgi on 4/04/2016.
 */


import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import tests.groups.Sanity;

//@Category(Sanity.class)
public class FirefoxTest {

    @Test
    public void startWebDriver(){

        WebDriver driver = new FirefoxDriver();

        driver.navigate().to("http://google.com");

        Assert.assertTrue("Title should start differently",
                driver.getTitle().startsWith("Google"));

        driver.close();
        driver.quit();
    }
}

