package sanity;

import org.junit.*;
import org.junit.experimental.categories.Category;
import tests.groups.Sanity;
import utilities.Config;
import utilities.Log;

/**
 * Created by kanak.kalburgi on 9/02/2016.
 */

@Category({Sanity.class})
public class Log4JTest {


    @BeforeClass
    public static void setUp() {

        Log.startTestCase("Log4J Test");

    }

    @Before
    public void testBefore() {

        Log.startTestCase("Before Method");

    }

    @After
    public void testAfter() {

        Log.startTestCase("After Method");

    }


    @Test
    public void successCheck() {

        Log.info("Test peacefully in progrezz");

    }

    @AfterClass
    public static void tearDown() {
        Log.endTestCase("Log4J Test");
    }
}
