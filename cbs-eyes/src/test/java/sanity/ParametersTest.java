package sanity;

/**
 * Created by kanak.kalburgi on 4/05/2016.
 */

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import tests.groups.Debug;
import utilities.Config;
import utilities.GlobalLog;
import utilities.Log;

@Category(Debug.class)
@RunWith(Parameterized.class)
public class ParametersTest {
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { "Scenario-001", "hostname" }
        });
    }

    private String scenario;
    private String host;

    public ParametersTest(String scenario, String host) {
        this.scenario = scenario;
        this.host = host;
    }

    @Test
    public void test() {

        Log.printConsole("Found Scenario: " + scenario, true);
    }
}

