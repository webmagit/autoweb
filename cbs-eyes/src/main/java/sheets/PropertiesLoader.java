package sheets;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
    private static final String PROPERTIES_FILE_PATH = "resources/config.properties";

    private InputStream input = null;

    public final Properties load() throws IOException {
        Properties properties = new Properties();
        InputStream input;

        try {
            input = new FileInputStream(PROPERTIES_FILE_PATH);

            properties.load(input);

            return properties;

        } catch (IOException ex) {
            ex.printStackTrace();

            dispose();

            throw ex;
        }
    }

    public final void dispose() {
        if (input != null) {
            try {
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
