package sheets;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class AuthorizationUtil {
    private static final String GOOGLE_API_HOST = "https://www.googleapis.com/";

    private static final String REDIRECT_URL = "urn:ietf:wg:oauth:2.0:oob";
    private static final String SCOPE = "https://spreadsheets.google.com/feeds "
            + "https://docs.google.com/feeds "
            + "https://www.googleapis.com/auth/drive "
            + "https://www.googleapis.com/auth/drive.appdata "
            + "https://www.googleapis.com/auth/drive.file "
            + "https://www.googleapis.com/auth/drive.apps.readonly "
            + "https://www.googleapis.com/auth/drive.photos.readonly";

    private static final String OFFLINE_ACCESS_TYPE = "offline";

    private static final String GOOGLE_AUTH_URL = "https://accounts.google.com/o/oauth2/auth";

    private static final String ACCEPT_HEADER_NAME = "Accept";

    private static final String OAuthScopeKey = "scope";
    private static final String OAuth2ClientId = "client_id";
    private static final String OAuth2RedirectUri = "redirect_uri";
    private static final String OAuth2AccessType = "access_type";
    private static final String OAuth2ResponseType = "response_type";
    private static final String OAuth2ApprovalPrompt = "approval_prompt";

    public String generateRefreshToken(String accessCode) throws Exception {
        OAuth2Parameters parameters = loadAuthorizationParameters();

        HttpClient client = HttpClients.createDefault();

        String url = String.format(
                "%soauth2/v3/token?client_id=%s&client_secret=%s&code=%s&grant_type=authorization_code&redirect_uri=%s",
                GOOGLE_API_HOST,
                parameters.clientId,
                parameters.clientSecret,
                accessCode,
                REDIRECT_URL
        );
        HttpPost post = new HttpPost(url);
        post.addHeader(ACCEPT_HEADER_NAME, "application/x-www-form-urlencoded");

        try {
            HttpResponse response = client.execute(post);

            JSONObject object = readJson(response);

            return object.getString("refresh_token");
        }
        finally {
            post.releaseConnection();
        }
    }

    public String createAuthorizationUrl() throws IOException {
        OAuth2Parameters parameters = loadAuthorizationParameters();

        return buildAuthorizationUrl(GOOGLE_AUTH_URL, parameters);
    }

    private OAuth2Parameters loadAuthorizationParameters() throws IOException {
        OAuth2Parameters parameters = new OAuth2Parameters();

        PropertiesLoader loader = new PropertiesLoader();
        Properties properties = loader.load();

        parameters.clientId = properties.getProperty("google.clientId");
        parameters.clientSecret = properties.getProperty("google.clientSecret");

        loader.dispose();

        parameters.redirectUri = REDIRECT_URL;

        parameters.scope = SCOPE;

        parameters.accessType = OFFLINE_ACCESS_TYPE;

        parameters.responseType = "code";
        parameters.tokenType = "Bearer";
        parameters.approvalPrompt = "auto";

        return parameters;
    }

    private String buildAuthorizationUrl(String authUrl, OAuth2Parameters parameters) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(authUrl);

        stringBuilder.append(String.format("?%s=%s", OAuth2ResponseType, parameters.responseType));
        stringBuilder.append(String.format("&%s=%s", OAuth2ClientId, parameters.clientId));
        stringBuilder.append(String.format("&%s=%s", OAuth2RedirectUri, parameters.redirectUri));
        stringBuilder.append(String.format("&%s=%s", OAuthScopeKey, parameters.scope));
        stringBuilder.append(String.format("&%s=%s", OAuth2AccessType, parameters.accessType));
        stringBuilder.append(String.format("&%s=%s", OAuth2ApprovalPrompt, parameters.approvalPrompt));

        return stringBuilder.toString();
    }

    private JSONObject readJson(HttpResponse response) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            result.append(line);
        }

        return new JSONObject(result.toString());
    }

    private class OAuth2Parameters {
        public String responseType ;
        public String clientId;
        public String clientSecret;
        public String redirectUri;
        public String scope;
        public String accessType;
        public String approvalPrompt;
        public String tokenType;
    }
}
