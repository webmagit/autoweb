package sheets;

import java.io.*;
import java.util.Properties;

public class Program
{
    private static final String AUTH_PARAM = "auth";

    private static String spreadSheetId;
    private static int projectId;
    private static String storiesFilter;

    public static void main(String[] args) throws IOException
    {
        PrintStream out = System.out;
        Console console = System.console();

        /*try {
            generateGoogleRefreshToken();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        try
        {
            if (args.length >= 0) {
//                if(AUTH_PARAM.equals(args[0])) {
                    if(true) {
                    generateGoogleRefreshToken();

                    return;
                }

                spreadSheetId = args[0];
            }

            if (spreadSheetId == null) {
                out.println("Please enter the spreadsheet ID:");

                spreadSheetId = console.readLine();

                if ("".equals(spreadSheetId)) {
                    throw new IllegalStateException("The id of the spreadsheet should not be empty");
                }
            }

            loadProperties();

            out.println();
            out.println("Please wait..");

            readKey();
        }
        catch (Exception error) {
            out.println(error.getMessage());

            readKey();
        }
    }

    private static void readKey() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void loadProperties() throws IOException {
        PropertiesLoader loader = new PropertiesLoader();
        Properties properties = loader.load();

        projectId = Integer.parseInt(properties.getProperty("pivotal.projectId"));
        storiesFilter = properties.getProperty("pivotal.storiesFilter");

        loader.dispose();
    }

    private static void generateGoogleRefreshToken() throws Exception {
        AuthorizationUtil util = new AuthorizationUtil();
//        String url = util.createAuthorizationUrl();

//        https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=blah&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope=https://spreadsheets.google.com/feeds https://docs.google.com/feeds https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/drive.appdata https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/drive.apps.readonly https://www.googleapis.com/auth/drive.photos.readonly&access_type=offline&approval_prompt=auto

//        System.out.println("1. Please go to this url in your browser:");

//        System.out.println(url);

//        System.out.println("2. Press 'Allow Access' & copy the provided access code. Paste it here:");

//        String accessCode = System.console().readLine();
        String accessCode = "4/tfgjgIWdIp7BSQM9ulJsB9zg5bP-70D1UbWebtqpEgE";

        String refreshToken = util.generateRefreshToken(accessCode);

        System.out.println("Your refresh token is:");
        System.out.println(refreshToken);

        System.out.println("Please copy your token & save in configuration file");

        readKey();
    }
}
